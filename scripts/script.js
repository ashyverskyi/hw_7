const toggleCheckboxLink = document.getElementById("toggleCheckbox");
const myCheckbox = document.getElementById("myCheckbox");
toggleCheckboxLink.addEventListener("click", function (event) {
    event.preventDefault();
    myCheckbox.checked = !myCheckbox.checked;
});

